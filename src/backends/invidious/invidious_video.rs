use crate::backends::common::video::*;
use glib::{clone, MainContext, ParamSpec, Properties, Value};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use invidious::{self, ClientAsyncTrait};
use regex::Regex;
use std::cell::RefCell;
use std::collections::HashSet;

mod imp {
    use super::*;

    #[derive(Default, Properties)]
    #[properties(wrapper_type = super::SharitInvidiousVideo)]
    pub struct SharitInvidiousVideo {
        #[property(get, set)]
        id: RefCell<Option<String>>,
        #[property(get, set)]
        instance: RefCell<Option<String>>,

        pub client: RefCell<invidious::ClientAsync>,
        pub widget: Option<gtk::GLArea>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SharitInvidiousVideo {
        const NAME: &'static str = "SharitInvidiousVideo";
        type Type = super::SharitInvidiousVideo;
        type ParentType = gtk::Widget;
    }

    impl ObjectImpl for SharitInvidiousVideo {
        // properties
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }

        fn constructed(&self) {
            self.parent_constructed();

            if let Some(inst) = self.instance.borrow().as_ref() {
                self.client.borrow_mut().set_instance(inst.clone());
            }

            self.obj().connect_instance_notify(|obj| {
                if let Some(inst) = obj.imp().instance.borrow().as_ref() {
                    obj.imp().client.borrow_mut().set_instance(inst.clone());
                } else {
                    obj.imp().client.replace(invidious::ClientAsync::default());
                };

                let main_context = MainContext::default();
                #[allow(unused_braces)]
                main_context
                    .spawn_local(clone!(@weak obj => async move { obj.update_widget().await }));
            });

            self.obj().connect_id_notify(|obj| {
                let main_context = MainContext::default();
                #[allow(unused_braces)]
                main_context
                    .spawn_local(clone!(@weak obj => async move { obj.update_widget().await }));
            });
        }
    }

    impl WidgetImpl for SharitInvidiousVideo {}

    impl GLAreaImpl for SharitInvidiousVideo {}
}

glib::wrapper! {
    pub struct SharitInvidiousVideo(ObjectSubclass<imp::SharitInvidiousVideo>)
        @extends gtk::GLArea, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl SharitInvidiousVideo {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn from(instance: Option<String>, id: Option<String>) -> Self {
        glib::Object::builder()
            .property("id", id)
            .property("instance", instance)
            .build()
    }

    async fn update_widget(&self) {
        if let Some(id) = self.id() {
            // TODO: remove unwrap
            let video = self.imp().client.borrow().video(&id, None).await.unwrap();

            let re = Regex::new(r#"^(video|audio)\/([a-zA-Z0-9]+); codecs="([a-zA-Z0-9\.]+)"$"#)
                .unwrap();

            let encs: HashSet<Stream> = video
                .adaptive_formats
                .iter()
                .filter(|s| re.is_match(&s.r#type))
                .filter_map(|s| {
                    let caps = re.captures(&s.r#type).unwrap();

                    if <String as TryInto<Container>>::try_into(caps[1].to_string()).is_err() {
                        return None;
                    }

                    match &caps[0] {
                        "video" => {
                            if <String as TryInto<VideoCodec>>::try_into(caps[2].to_string())
                                .is_err()
                            {
                                return None;
                            }

                            Some(Stream::Video(VideoStream {
                                url: s.url.clone(),
                                container: caps[1].to_string().try_into().unwrap(),
                                codec: caps[2].to_string().try_into().unwrap(),
                                resolution: s.resolution[..s.resolution.len() - 1]
                                    .parse::<usize>()
                                    .unwrap(),
                                fps: s.fps.into(),
                            }))
                        }
                        "audio" => {
                            if <String as TryInto<AudioCodec>>::try_into(caps[2].to_string())
                                .is_err()
                            {
                                return None;
                            }

                            Some(Stream::Audio(AudioStream {
                                url: s.url.clone(),
                                container: caps[1].to_string().try_into().unwrap(),
                                codec: caps[2].to_string().try_into().unwrap(),
                                bitrate: s.bitrate.parse::<usize>().unwrap(),
                            }))
                        }
                        _ => panic!("impossible"),
                    }
                })
                .collect();

            let mut vencs: Vec<VideoStream> = encs
                .iter()
                .filter_map(|e| match e {
                    Stream::Video(v) => Some(v.clone()),
                    Stream::Audio(_) => None,
                })
                .collect();
            vencs.sort();

            let mut aencs: Vec<AudioStream> = encs
                .iter()
                .filter_map(|e| match e {
                    Stream::Video(_) => None,
                    Stream::Audio(a) => Some(a.clone()),
                })
                .collect();
            aencs.sort();

            /*
            let (venc, vcodec) = if encs.contains("vp9") {
                ("vp9", "vp9dec")
            } else {
                // TODO: remove panic
                panic!();
            };

            let (aenc, acodec) = if encs.contains("opus") {
                ("opus", "opusdec")
            } else if encs.contains("aac") {
                ("aac", "avdec_aac")
            } else {
                // TODO: remove panic
                panic!();
            };
            */
        }
    }
}
