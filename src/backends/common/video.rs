use std::cmp::Ordering;

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum Stream {
    Audio(AudioStream),
    Video(VideoStream),
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct AudioStream {
    pub url: String,
    pub container: Container,
    pub codec: AudioCodec,
    pub bitrate: usize,
}

impl PartialOrd for AudioStream {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.bitrate != other.bitrate {
            self.bitrate.partial_cmp(&other.bitrate)
        } else {
            self.codec.partial_cmp(&other.codec)
        }
    }
}

impl Ord for AudioStream {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub struct VideoStream {
    pub url: String,
    pub container: Container,
    pub codec: VideoCodec,
    pub resolution: usize,
    pub fps: usize,
}

impl PartialOrd for VideoStream {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.resolution != other.resolution {
            self.resolution.partial_cmp(&other.resolution)
        } else if self.fps != other.fps {
            self.fps.partial_cmp(&other.fps)
        } else {
            self.codec.partial_cmp(&other.codec)
        }
    }
}

impl Ord for VideoStream {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum Container {
    MP4,
    WebM,
}

impl TryFrom<String> for Container {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "mp4" => Ok(Self::MP4),
            "webm" => Ok(Self::WebM),
            _ => Err(value),
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub enum AudioCodec {
    OPUS = 2,
    M4A = 1,
}

impl AudioCodec {
    pub fn to_gst_element(ac: Self) -> String {
        match ac {
            Self::OPUS => "opusdec".to_string(),
            Self::M4A => "avdec_aac".to_string(),
        }
    }
}

impl TryFrom<String> for AudioCodec {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.starts_with("opus") {
            Ok(Self::OPUS)
        } else if value.starts_with("mp4a") {
            Ok(Self::M4A)
        } else {
            Err(value)
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub enum VideoCodec {
    AV1 = 3,
    VP9 = 2,
    AVC = 1,
}

impl VideoCodec {
    pub fn to_gst_element(ac: Self) -> String {
        match ac {
            Self::AV1 => "av1dec".to_string(),
            Self::VP9 => "vp9dec".to_string(),
            Self::AVC => "avdec_mpeg4".to_string(),
        }
    }
}

impl TryFrom<String> for VideoCodec {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.starts_with("av01") {
            Ok(Self::AV1)
        } else if value.starts_with("vp9") {
            Ok(Self::VP9)
        } else if value.starts_with("avc1") {
            Ok(Self::AVC)
        } else {
            Err(value)
        }
    }
}
