pub static APP_ID: &str = "eu.lyes.Sharit";
pub static VERSION: &str = "0.1.0";
pub static PROFILE: &str = "devel";
pub static GETTEXT_PACKAGE: &str = "sharit";
pub static LOCALEDIR: &str = "/usr/local/share/locale";
pub static PKGDATADIR: &str = "/usr/local/share/sharit";
