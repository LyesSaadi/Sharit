use gtk::subclass::prelude::*;
use gtk::prelude::*;
use gtk::{glib, CompositeTemplate};
use glib::{ParamSpec, Properties, Value};
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/lyes/Sharit/widgets/player.ui")]
    #[properties(wrapper_type = super::SharitPlayer)]
    pub struct SharitPlayer {
        #[property(get, set)]
        video: RefCell<Option<gtk::GLArea>>,

        #[template_child]
        pub video_thumbnail_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub thumbnail_page: TemplateChild<gtk::Overlay>,
        #[template_child]
        pub thumbnail: TemplateChild<gtk::Image>,
        #[template_child]
        pub video_page: TemplateChild<gtk::Overlay>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SharitPlayer {
        const NAME: &'static str = "SharitPlayer";
        type Type = super::SharitPlayer;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SharitPlayer {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }

        fn constructed(&self) {
            self.parent_constructed();

            self.obj().set_layout_manager(Some(gtk::BinLayout::new()));

            self.video_thumbnail_stack.set_visible_child(&self.thumbnail_page.get());
        }
    }

    impl WidgetImpl for SharitPlayer {}
}

glib::wrapper! {
    pub struct SharitPlayer(ObjectSubclass<imp::SharitPlayer>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

#[gtk::template_callbacks]
impl SharitPlayer {
    pub fn new() -> Self {
        glib::Object::new()
    }

    #[template_callback]
    pub fn start(&self) {}
    #[template_callback]
    pub fn play(&self) {}
    #[template_callback]
    pub fn pause(&self) {}
    #[template_callback]
    pub fn on_time_scale_scroll(&self) {}
    #[template_callback]
    pub fn on_volume_change(&self) {}
    #[template_callback]
    pub fn show_subtitles(&self) {}
}

